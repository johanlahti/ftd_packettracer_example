from netmiko.cisco import CiscoFtdSSH
from rulestatus import RuleStatus

# Instansiera ett objekt för att kommunicera med FTD CLI
session = CiscoFtdSSH(
                      ip="10.10.20.65",
                      username="admin",
                      password="Cisco1234"
                      )

# Instansiera regeltestklassen med FTD-cli objektet som argument
ftd = RuleStatus(session)

# Använd kommandot verify för att testa ett flöde
response = ftd.verify(
            source_ip = "1.1.1.1",
            dest_ip = "2.2.2.2",
            dest_port = 443,
            input_interface = "inside",
            protocol = "tcp"
)

# Returnerar True eller False baserat på om det finns regel som tillåter
if response is True:
    print("Fungerar!")
else:
    print("Nåt är paj!")
