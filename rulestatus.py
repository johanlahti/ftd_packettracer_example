import re

class RuleStatus:

    def __init__(self, session):
        self.session = session

    def verify(
               self,
               input_interface: str = "",
               source_ip: str = "",
               dest_ip: str = "",
               protocol: str = "",
               source_port: int = 0,
               dest_port:int = 0
               ):

        params = {
               "input_interface": input_interface,
               "source_ip": source_ip,
               "dest_ip": dest_ip,
               "protocol": protocol,
               "source_port": source_port,
               "dest_port": dest_port
        }

        return self._check(params)

    def _check(self, params: dict):
        print("Testar packettracer för:")
        for k, v in params.items():
            print(f"\t-{k} = {v}")


        command = self._construct_cli_command(params)
        print(f"Skickar cli-kommando: {command}")

        raw_response = self.session.send_command(command)
        action = self._get_action_from_response(raw_response)
        return self._allow_or_drop(action)

    def _construct_cli_command(self, p: dict) -> str:
        command = (f"packet-tracer input {p.get('input_interface')} "
                   f"{p.get('protocol')} {p.get('source_ip')} "
                   f"{p.get('source_port')} {p.get('dest_ip')} {p.get('dest_port')}"
                  )
        return command

    def _allow_or_drop(self, data: str) -> bool:
        if re.match(r'permit', data):
            return True
        else:
            return False

    def _get_action_from_response(self, data: str) -> str:
        """
        Returns Action from response.
        """
        res_lines = data.splitlines()
        for line in res_lines:
            d = re.match(r'Action.*', line)
            if d:
                return d.string
